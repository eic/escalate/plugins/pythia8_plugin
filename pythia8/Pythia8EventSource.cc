#include <iostream>
#include <memory>
#include <string>

#include <fmt/format.h>     // For format and print functions

#include <JANA/JEventSourceGeneratorT.h>
#include <JANA/JSourceFactoryGenerator.h>

#include <MinimalistModel/McGeneratedParticle.h>

#include "Pythia8EventSource.h"

#include <ejana/TextEventFileReader.h>
#include <ejana/EStringHelpers.h>


using namespace ej;
using namespace minimodel;

//----------------
// Constructor
//----------------
Pythia8EventSource::Pythia8EventSource(const std::string& source_name, JApplication *app):
    JEventSource(source_name, app)
{
    using namespace fmt;

    // Open file
    print("Pythia8EventSource: Opening configuration file '{}'\n", source_name);

    //Make factory generator that will make factories for all types provided by the event source
    //This is necessary because the JFactorySet needs all factories ahead of time
    //Make sure that all types are listed as template arguments here!!
    SetFactoryGenerator(new JSourceFactoryGenerator<>());

    // Event index = number of event read
    _entry_index = 0;

    _control = std::make_unique<ej::EventSourceControl>(app->GetJParameterManager());

    // Arguments
    _verbose = 1;                // verbose output level 0=none, 1=some, 2=verbose
    _hepmc_out_name = "";
    
    auto pm = app->GetService<JParameterManager>();
    pm->SetDefaultParameter("pythia8:verbose", _verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");
    pm->SetDefaultParameter("pythia8:hepmc_out", _hepmc_out_name, "Track fitting algorithm. Options are 'genfit' or 'acts'");

    if(!_hepmc_out_name.is_empty()) {
        // Interface for conversion from Pythia8::Event to HepMC event.
        HepMC::Pythia8ToHepMC ToHepMC;

        // Specify file where HepMC events will be stored.
        HepMC::IO_GenEvent ascii_io(_hepmc_out_name, std::ios::out);

        // Switch off warnings for parton-level events.
        ToHepMC.set_print_inconsistency(false);
        ToHepMC.set_free_parton_exception(false);
    }

    // Read in commands from external file.
    pythia.readFile(source_name);

    // Initialize.
    pythia.init();
}

//----------------
// GetEvent
//----------------
void Pythia8EventSource::GetEvent(std::shared_ptr<JEvent> event)
{
    if (!pythia.next()) continue;
    

    // Construct new empty HepMC event and fill it.
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;
}
