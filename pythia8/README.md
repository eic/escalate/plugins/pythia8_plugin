# LUND file format

Lund file format has many flavours. It consist of one line of the event record and
a line for each particle. 

```
event 1 data
prticle 1 record
prticle 2 record
...
event 2 data
particle 1 record
particle 2 record
... 
```

## Particle record
|column|quantity|
|------|--------|
|1|index|
|2|Lifetime [nanoseconds] (UD)|
|3|type (1 is active)|
|4|particle ID|
|5|Index of the parent (UD)|
|6|Index of the first daughter (UD)|
|7|momentum x   [GeV]|
|8|momentum y   [GeV]|
|9|momentum z   [GeV]|
|10|Energy of the particle [GeV] (UD)|
|11|Mass of the particle [GeV] (UD)|
|12|vertex x [cm]|
|13|vertex y [cm]|
|14|vertex z [cm]|

In ejana, this record is represented by [LundParticle](LundParticle.h) class;

## Event record:

While particle record is more or less standard, the event
record varies greatly depending on generator. 

(which is
usually pretty custom depending on the generator)

The header of Pythia6 is:

|column|quantity|
|------|--------|
|0|Number of particles|
|1|Mass number of the target (UD)|
|2|Atomic number oif the target (UD)|
|3|Target polarization  (UD)|
|4|Beam Polarization|
|5|Beam type, electron=11, photon=22” (UD)|
|6|Beam energy (GeV)  (UD)|
|7|Interacted nucleon ID (2212 or 2112)  (UD)|
|8|Process ID (UD)|
|9|Event weight (UD)|

But for example [EIC_mesonMC](https://github.com/JeffersonLab/EIC_mesonMC) uses 
fields 2-5 ot represent true DIS info (x, Q2, nu, etc.)

In ejana lund event data is represented by [LundEventData](LundEventData.h) class, 
Which has `LundEventData::column_values`. The most important members are: 

```c++
    class LundEventData : public JObject {
    public:
        // FIELDS

        /// Values for the lund header
        ///
        /// In original pythia6 format: 
        ///    column_values[0] - the target mass, 
        ///    column_values[8] - Event weight
        std::vector<double> column_values;

        /// Collection of primary vertexes (that came from a generator)
        std::vector<LundParticle *> particles; // Primary particles (that comes from a generator)

        /// Not parsed event (in forms of tokens)
        std::unique_ptr<ej::TextFileEvent> text_event;

        /// Parse() function was called
        bool is_parsed = false;

        /// File line number at which this event starts
        uint64_t started_at_line;
// ...
```

Thus, the length 

So it might be used as (the example is for [EIC_mesonMC]):

```c++
auto event_data = event->GetSingle<ej::LundEventData>();
tgt_mass   = eventData->column_values[1];   // Column #2  Mass number of the target
evt_weight = eventData->column_values[9];   // Column #10 Event weight
```


### Using Lund files in analysis

It is recommended to organize the analysis code so that it shouldn't know where 
the data comes from. LundParticle-s are correctly converted to the general
McGeneratedParticle class;

But because lund event format could be customized and analysis 
may need the data, here is a code sample which checks and uses LundEventData directly:

```c++

bool is_lund_evnet = event->GetFactory<ej::LundEventData>("", false) != nullptr;

if(is_lund_evnet) {
    have_true_dis_info = true;
    auto eventData = event->GetSingle<ej::LundEventData>();
    true_x = eventData->column_values[0];
    true_q2 = eventData->column_values[1];
    // ...
```


 