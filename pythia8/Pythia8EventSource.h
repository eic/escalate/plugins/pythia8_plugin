#ifndef PYTHIA8_EVENT_SROUCE_HEADER
#define PYTHIA8_EVENT_SROUCE_HEADER

#include <JANA/JEventSource.h>
#include <JANA/JEvent.h>

#include <Pythia8/Pythia.h>
#include <ejana/EventSourceControl.h>


class Pythia8EventSource: public JEventSource {
public:

    explicit Pythia8EventSource(const std::string& source_name, JApplication *app = nullptr);
    ~Pythia8EventSource() override = default;

    /** Get description of this event source */
    std::string GetVDescription() const override {return GetDescription(); }

    // A description of this source type must be provided as a static member
    static std::string GetDescription() { return "Text file in the LUND format"; }

    /** Reads next event and returns it as JEvent from */
    void GetEvent(std::shared_ptr<JEvent>) override;

    /** Provides a factory with objects */
    bool GetObjects(const std::shared_ptr<const JEvent>&, JFactory* ) override {return false;}

    protected:
        int64_t _entry_index;        // int64 but not uint64 because of the root GetEntries


    private:
        Pythia8::Pythia _pythia;
        std::unique_ptr<ej::EventSourceControl> _control;
        int _verbose = 1;                // verbose output level 0=none, 1=some, 2=verbose
        std::string _hepmc_out_name = "";
};

#endif // _JEventSourceGenerator_jleic_root_

